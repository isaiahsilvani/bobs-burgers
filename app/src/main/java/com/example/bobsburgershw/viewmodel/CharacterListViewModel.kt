package com.example.bobsburgershw.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.bobsburgershw.model.CharacterRepo
import com.example.bobsburgershw.model.local.model.Character
import com.example.bobsburgershw.view.CharacterList.CharacterListState
import kotlinx.coroutines.launch

class CharacterListViewModel(private val repo: CharacterRepo) : ViewModel() {

    private val _characterListState: MutableLiveData<CharacterListState> = MutableLiveData(
        CharacterListState()
    )
    val characterListState: LiveData<CharacterListState> get() = _characterListState

    // Get the characters from repo, set in the state
    fun getCharacters(count: Int) {
        viewModelScope.launch {
            _characterListState.value = CharacterListState(isLoading = true)
            _characterListState.value =
                CharacterListState(
                    characterList = repo.getCharacters(count),
                    isLoading = false,
                )
            Log.e("TAG UPDATED", _characterListState.value.toString())
        }
    }
    // Set the favorite character when user clicks character card in Recycler View
    fun onSetFavorite(character: Character) {
        viewModelScope.launch {
            val updatedList = _characterListState.value?.characterList
            Log.e("VM", updatedList.toString())
            repo.updateCharacter(character)
        }
    }
    // Get more characters, make an API call without checking database
    fun getMoreCharacters() {
        viewModelScope.launch {
            var result: List<Character>
            with(_characterListState) {
                val characterList = value?.characterList
                result = if (!characterList.isNullOrEmpty()) {
                    val lastId = characterList[characterList.size - 1].id
                    repo.getMoreCharacters(lastId)
                } else {
                    repo.getMoreCharacters(0)
                }
                value = CharacterListState(characterList = result, pressedAddMore = true)
            }
        }
    }
    // Delete all characters in the database, clear character list in state
    fun deleteAllCharacters() {
        viewModelScope.launch {
            repo.deleteAllCharacters()
            _characterListState.value = CharacterListState(characterList = emptyList())
        }
    }
    // Delete a specific character in database
    fun deleteCharacter(character: Character) {
        viewModelScope.launch {
            repo.deleteCharacter(character)
        }
    }
    // Retrieve favorite characters from database, set the state to those favorite characters only
    fun showFavorites() {
        with(_characterListState) {
            viewModelScope.launch {
                // Filter the existing list to update state with favorite characters only
                if (value?.showFavorites == true) {
                    // Switch back to Characters without needing a whole different fragment
                    _characterListState.value = CharacterListState(
                        characterList = repo.getCharacters(20),
                        showFavorites = false
                    )
                } else {
                    val favorites = repo.getFavoriteCharacters()

                    for (character in favorites) {
                        Log.e("VM", "${character.name} is favorite - ${character.isFavorite}")
                    }
                    value = value?.copy(characterList = favorites, showFavorites = true)
                }

            }
        }
    }
}