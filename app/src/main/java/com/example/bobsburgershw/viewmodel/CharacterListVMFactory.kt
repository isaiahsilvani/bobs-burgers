package com.example.bobsburgershw.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.bobsburgershw.model.CharacterRepo


class CharacterListVMFactory(
    private val repo: CharacterRepo
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return CharacterListViewModel(repo) as T
    }
}

