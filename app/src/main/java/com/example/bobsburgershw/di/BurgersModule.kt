package com.example.bobsburgershw.di

import android.content.Context
import com.example.bobsburgershw.model.CharacterRepo
import com.example.bobsburgershw.model.local.CharacterDB
import com.example.bobsburgershw.model.local.CharacterDao
import com.example.bobsburgershw.model.local.model.Character
import com.example.bobsburgershw.model.remote.CharacterService
import com.example.bobsburgershw.viewmodel.CharacterListVMFactory
import com.example.bobsburgershw.viewmodel.CharacterListViewModel
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
class BurgersModule {

    // View Model
    @Provides
    @Singleton
    fun providesCharacterViewModel(vmFactory: CharacterListVMFactory) : CharacterListViewModel = vmFactory.create(CharacterListViewModel::class.java)

    // View Model Factory
    @Provides
    @Singleton
    fun providesCharacterViewModelFactory(repo: CharacterRepo): CharacterListVMFactory = CharacterListVMFactory(repo)

    // REPO
    @Provides
    @Singleton
    fun providesCharacterRepo(
        characterDao: CharacterDao,
        characterService: CharacterService
    ): CharacterRepo = CharacterRepo(characterService, characterDao)

    // Service
    @Provides
    @Singleton
    fun providesCharacterService(): CharacterService = CharacterService.getInstance()

    // DAO
    @Provides
    @Singleton
    fun providesDao(characterDB: CharacterDB): CharacterDao = characterDB.characterDao()

    // ROOM Database
    @Provides
    @Singleton
    fun providesDatabase(@ApplicationContext context: Context): CharacterDB = CharacterDB.getInstance(context)

}