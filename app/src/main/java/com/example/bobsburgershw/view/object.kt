package com.example.bobsburgershw.view

import com.example.bobsburgershw.view.CharacterList.CharacterListState


object getShowFavorites{

         fun getShowFavorites(state: CharacterListState): Boolean {
            return state.showFavorites
        }
}