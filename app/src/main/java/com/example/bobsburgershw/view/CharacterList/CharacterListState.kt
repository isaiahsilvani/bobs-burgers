package com.example.bobsburgershw.view.CharacterList

import com.example.bobsburgershw.model.local.model.Character

data class CharacterListState(

    var characterList: List<Character> = emptyList(),
    var isLoading: Boolean = false,
    var showFavorites: Boolean = false,
    var pressedAddMore: Boolean = false

)