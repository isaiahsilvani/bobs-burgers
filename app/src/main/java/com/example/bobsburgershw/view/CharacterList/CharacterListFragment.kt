package com.example.bobsburgershw.view.CharacterList

import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.example.bobsburgershw.R
import com.example.bobsburgershw.databinding.FragmentCharacterListBinding
import com.example.bobsburgershw.model.CharacterRepo
import com.example.bobsburgershw.model.local.model.Character
import com.example.bobsburgershw.viewmodel.CharacterListVMFactory
import com.example.bobsburgershw.viewmodel.CharacterListViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class CharacterListFragment : Fragment() {

    private var favorite = false

    @Inject
    lateinit var viewModel: CharacterListViewModel


    private val adapter by lazy {
        CharacterAdapter(
            viewModel::onSetFavorite,
            viewModel::deleteCharacter,
            requireContext(),
            this::scrollToBottom,
        )
    }

    lateinit var binding: FragmentCharacterListBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCharacterListBinding.inflate(layoutInflater)
        // Set up Recycler View
        viewModel.getCharacters(20)
        binding.rvCharacters.adapter = adapter
        binding.rvCharacters.layoutManager =
            GridLayoutManager(this@CharacterListFragment.context, 2)
        initObservers()
        setHasOptionsMenu(true)
        viewModel.getCharacters(20)
        return binding.root
    }
    // Define the Action bar at the top
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.delete_menu, menu)
    }
    // Define the behavior when a user clicks an icon in the action bar
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.menu_delete) {
            deleteAllCharacters()
        }
        return super.onOptionsItemSelected(item)
    }
    // Listen to changes in state for the character view model
    private fun initObservers() {
        viewModel.characterListState.observe(viewLifecycleOwner) { state ->
            Log.e("FRAG", state.characterList.toString())
            adapter.setData(state.characterList, state.pressedAddMore)
            with(binding) {
                // If showFavorites is enabled, when onButton click, get ALL the characters, change text
                btnFavorites.text = if (state.showFavorites) "Characters" else "Favorites"
                btnGetMoreCharacters.visibility = if (state.showFavorites) View.INVISIBLE else View.VISIBLE
                // if characterList is empty and showFavorites is false, make the button invisible
                if (!state.showFavorites && state.characterList.isEmpty()) {
                    btnFavorites.visibility = View.INVISIBLE
                } else {
                    btnFavorites.visibility = View.VISIBLE
                }
                // If characterList is empty, hide the delete all button in menu bar
                Log.e("STATE", state.characterList.toString())
                // A function that takes the state and return it to the adapter
                adapter.setShowFavorite(state.showFavorites)
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        with(binding) {

            btnGetMoreCharacters.setOnClickListener {
                Log.e("FRAGG", "HIT BTN GO TO FAV")
                viewModel.getMoreCharacters()
            }

            btnFavorites.setOnClickListener {
                Log.e("TAGG", "WTFFFF")
                viewModel.showFavorites()
            }
        }
    }
    // Functionality for deleting all characters
    private fun deleteAllCharacters() {
        // All the logic for deleting a single user. Create a dialouge builder
        // todo Research the dialouge builder
        val builder = AlertDialog.Builder(requireContext())
        builder.setPositiveButton("Yes") { _, _ ->
            viewModel.deleteAllCharacters()
            Toast.makeText(requireContext(), "All characters deleted", Toast.LENGTH_LONG).show()
        }
        builder.setNegativeButton("No") { _, _ -> }
        builder.setTitle("Delete All Characters")
        builder.setMessage("Are you sure you want to delete all the characters?")
        builder.create().show()
    }

    fun scrollToBottom(position: Int) {
        binding.rvCharacters.smoothScrollToPosition(position)
    }
}