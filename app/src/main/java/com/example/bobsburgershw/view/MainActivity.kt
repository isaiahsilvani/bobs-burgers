package com.example.bobsburgershw.view

import androidx.appcompat.app.AppCompatActivity
import com.example.bobsburgershw.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity(R.layout.activity_main)