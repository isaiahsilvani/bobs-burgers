package com.example.bobsburgershw.view.CharacterList

import android.app.AlertDialog
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.bobsburgershw.R
import com.example.bobsburgershw.databinding.ItemCharacterBinding
import com.example.bobsburgershw.model.local.model.Character



class CharacterAdapter(
    // Methods from ViewModel class, in order to make database changes on RecyclerView item click
    val onSetFavorite: (Character) -> Unit,
    val onDeleteCharacter: (Character) -> Unit,
    private val context: Context,
    val scrollToBottom: (Int) -> Unit,
) :
    RecyclerView.Adapter<CharacterAdapter.CharacterViewHolder>() {

    var showFavorites = false

    private var characterList: MutableList<Character> = mutableListOf()
    // Define bindings to the viewHolder in order to manipulate views
    inner class CharacterViewHolder(private val binding: ItemCharacterBinding) :
        RecyclerView.ViewHolder(binding.root) {
        var characterName = binding.tvName
        var characterImage = binding.imgCharacter
        var favoriteBtn = binding.icFavorite
        var deleteBtn = binding.icDelete
        var infoBtn = binding.icArrow
        fun displayImage(image: String) {
            binding.imgCharacter.load(image)
        }
    }
    // Tell the adapter what layout you're using for the viewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharacterViewHolder {
        return CharacterViewHolder(
            ItemCharacterBinding.inflate(LayoutInflater.from(parent.context))
        )
    }
    // Actually pass relevant data to the item view
    override fun onBindViewHolder(holder: CharacterViewHolder, position: Int) {
        with(holder) {

            characterName.text = characterList[position].name
            displayImage(characterList[position].image)
            setInfoButton(infoBtn, position)
            setFavoriteButton(favoriteBtn, position)
            setDeleteButton(deleteBtn, position)
        }
    }

    fun setShowFavorite(setShowFavorite: Boolean) {
        showFavorites = setShowFavorite
    }

    override fun getItemCount(): Int {
        return characterList.size
    }

    fun setData(list: List<Character>, pressedAddMore: Boolean = false) {
        Log.e("ADAPTER", list.toString())
        // If old list is greater than new list, notify items have been added to end of the old list
        if (characterList.size < list.size) {
            characterList = list.toMutableList()
            notifyDataSetChanged()
        } else {
            notifyDataSetChanged()
        }
        characterList = list.toMutableList()
        Log.e("TAG", characterList.size.toString())
        // If user pressed the GetMorePeeps button, scroll to the bottom
        if (characterList.size != 0 && pressedAddMore) scrollToBottom(characterList.size - 1)
    }
    // Handle the set favorite button on each character card
    private fun setFavoriteButton(button: ImageView, position: Int) {
        with(characterList[position]) {
            // Set initial heart button color depending on if character isFavorite
            button.setImageResource(if (isFavorite) R.drawable.ic_favorite_filled else R.drawable.ic_favorite_border)
            button.setOnClickListener {
                // If clicked, set favorite in the database, or don't if its already favorited
                with(characterList[position]) {
                    button.setImageResource(if (isFavorite) R.drawable.ic_favorite_border else R.drawable.ic_favorite_filled)
                    isFavorite = !isFavorite
                    // Run block which Update the favorite character in character_table
                    onSetFavorite(this)
                    // TODO UPDATE THE STATE IF USER IS ON THE SHOWFAVORITE TAB
                    if (!isFavorite) {
                        characterList.remove(characterList[position])
                        notifyDataSetChanged()
                    }
                }
            }
        }
    }
    // Set up the Delete button functionality on each character card
    private fun setDeleteButton(button: ImageView, position: Int) {
        with(characterList[position]) {
            button.setOnClickListener {
                val builder = AlertDialog.Builder(context)
                builder.setPositiveButton("Yes") { _, _ ->

                    onDeleteCharacter(characterList[position])
                    val oldPosition = characterList.indexOf(this)
                    notifyDataSetChanged()
                    characterList.remove(this)
                    Toast.makeText(context, "$name deleted", Toast.LENGTH_LONG).show()
                }
                builder.setNegativeButton("No") { _, _ -> }
                builder.setTitle("Delete $name")
                builder.setMessage("Are you sure you want to delete $name?")
                builder.create().show()
            }
        }
    }
    // Set the Alert Dialogue for the info button on each character card
    private fun setInfoButton(button: ImageView, position: Int) {
        val currentCharacter = characterList[position]
        button.setOnClickListener {
            rotateButton(button)
            Log.e("ADAPTERRR", "HItTT")
            val builder = AlertDialog.Builder(context)
            builder.setNegativeButton("Go Back") { _, _ ->
                rotateButton(button)
            }
            builder.setTitle(currentCharacter.name)
            with(currentCharacter) {
                builder.setMessage(
                    "Gender: $gender\n" +
                            "Hair Color: $hairColor\n" +
                            "Occupation: $occupation\n" +
                            "Voiced By: $voicedBy"
                )
            }
            builder.setOnCancelListener {
                rotateButton(button)
            }
            builder.create().show()
        }
    }
    // Rotate the info button
    private fun rotateButton(button: ImageView) {
        // If button is already flipped, set it back to normal rotation position
        if (button.rotation == 180f) {
            button.animate().rotation(0f).setDuration(200).start()
        } else {
            // If button is NOT flipped, rotate it
            button.animate().rotation(180f).setDuration(200).start()
        }
    }

}