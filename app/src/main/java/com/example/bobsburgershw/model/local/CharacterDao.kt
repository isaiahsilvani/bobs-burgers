package com.example.bobsburgershw.model.local

import androidx.room.*
import com.example.bobsburgershw.model.local.model.Character
// Data Access Object
@Dao
interface CharacterDao {
    // Get ALL characters from the database
    @Query("SELECT * FROM character_table")
    suspend fun getAllCharacters(): List<Character>

    // Get ALL characters from the database
    @Query("SELECT * FROM character_table WHERE isFavorite = 1")
    suspend fun getFavoriteCharacters(): List<Character>

    // Delete ALL characters
    @Query("DELETE FROM character_table")
    suspend fun deleteAllCharacters()

    // Insert unlimited # of characters from a request
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCharacter(vararg character: Character)

    // Update the selected character
    @Update
    suspend fun updateCharacter(character: Character)

    // Delete the selected character
    @Delete
    suspend fun deleteCharacter(character: Character)
}