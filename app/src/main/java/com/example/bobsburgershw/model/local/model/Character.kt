package com.example.bobsburgershw.model.local.model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey

// Entity to create DB table AND our model for everything else
@Entity(tableName = "character_table")
data class Character(
    @PrimaryKey
    val id: Int = 0,
    val age: String = "N/a",
    val firstEpisode: String = "",
    val gender: String = "",
    val hairColor: String = "N/a",
    val image: String = "",
    val name: String  = "",
    val occupation: String  = "N/a",
    val voicedBy: String  = "",
    var isFavorite: Boolean = false
)