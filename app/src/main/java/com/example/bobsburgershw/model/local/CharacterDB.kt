package com.example.bobsburgershw.model.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.bobsburgershw.model.local.model.Character
// Basic ROOM database setup
@Database(entities = [Character::class], version = 1)
abstract class CharacterDB : RoomDatabase() {

    abstract fun characterDao(): CharacterDao

    companion object {
        private const val DATABASE_NAME = "character.db"

        @Volatile
        private var instance: CharacterDB? = null

        fun getInstance(context: Context): CharacterDB {
            return instance ?: synchronized(this) {
                instance ?: buildDatabase(context).also {
                    instance = it
                }
            }
        }

        private fun buildDatabase(context: Context): CharacterDB {
            return Room
                .databaseBuilder(context, CharacterDB::class.java, DATABASE_NAME)
                .build()
        }
    }
}