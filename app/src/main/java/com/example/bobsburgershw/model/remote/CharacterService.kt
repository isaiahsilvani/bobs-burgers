package com.example.bobsburgershw.model.remote

import com.example.bobsburgershw.model.local.model.Character
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import retrofit2.http.GET
import retrofit2.http.Query

interface CharacterService {

    companion object {
        // Endpoints as static variables
        private const val BASE_URL = "https://bobsburgers-api.herokuapp.com"
        private const val ENDPOINT_CHARACTERS = "/characters"

        // Get an instance by creating a Retrofit instance and using Builder
        fun getInstance(): CharacterService = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create()
    }
    // Get characters at start without skipping
    @GET(ENDPOINT_CHARACTERS)
    suspend fun getCharacters(@Query("limit") poop: Int): List<Character>

    // Get more unique characters by SKIPPING the amount of characters we already have
    // So API doesn't return same characters. Basically pagination
    @GET(ENDPOINT_CHARACTERS)
    suspend fun getMoreCharacters(
        @Query("skip") lastId: Int,
        @Query("limit") limit: Int = 10
    ): List<Character>

}