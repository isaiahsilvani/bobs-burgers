package com.example.bobsburgershw.model

import android.content.Context
import android.util.Log
import com.example.bobsburgershw.model.local.CharacterDB
import com.example.bobsburgershw.model.local.CharacterDao
import com.example.bobsburgershw.model.local.model.Character
import com.example.bobsburgershw.model.remote.CharacterService
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject


class CharacterRepo @Inject constructor(
    private val characterService: CharacterService,
    private val characterDao: CharacterDao
) {

    suspend fun getCharacters(count: Int, lastId: Int? = null) = withContext(Dispatchers.IO){
        // When count = 20, that means user is first starting the app. Get all characters from database
        val cachedCharacters = if (count == 20) {
            characterDao.getAllCharacters()
        } else {
            // If user can get favorite characters, characters must already be in the database. Just get them!
            characterDao.getFavoriteCharacters()
        }
        // If there are no characters in the database, make API call to fetch them, and insert into database
        return@withContext cachedCharacters.ifEmpty {
            // Store result from API call in local variable
            val remoteCharacters = characterService.getCharacters(count)
            // Store result in the database
            characterDao.insertCharacter(*remoteCharacters.toTypedArray())
            // Return the local variable since it is the same as database
            return@withContext remoteCharacters
        }
    }

    // GET more characters by making a API call without checking current database
    suspend fun getMoreCharacters(lastId: Int) = withContext(Dispatchers.IO) {
        // Store result from API call, then insert additional characters to database,
        // and return ALL characters in the database to the ViewModel
        val remoteCharacters = characterService.getMoreCharacters(lastId)
        characterDao.insertCharacter(*remoteCharacters.toTypedArray())

        return@withContext characterDao.getAllCharacters()
    }

    // ONLY get favorite characters from DB
    suspend fun getFavoriteCharacters() = withContext(Dispatchers.IO) {
        return@withContext characterDao.getFavoriteCharacters()
    }

    suspend fun deleteAllCharacters() = withContext(Dispatchers.IO) {
        characterDao.deleteAllCharacters()
    }

    suspend fun updateCharacter(character: Character) = withContext(Dispatchers.IO) {
        characterDao.updateCharacter(character)
    }

    suspend fun deleteCharacter(character: Character) = withContext(Dispatchers.IO) {
        characterDao.deleteCharacter(character)
    }
}